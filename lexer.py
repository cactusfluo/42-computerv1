#!/usr/bin/python3

################################################################################
##[ IMPORTS ]###################################################################
################################################################################

import  re;

################################################################################
##[ MODULE ]####################################################################
################################################################################

##################################################
def lexe(string): ################################
    ############################## 
    ##[ PROPERTIES ]##############
    ############################## 
    digits = "0123456789"
    tokens = [];
    token = "";
    i = 0;

    ############################## 
    ##[ LOCAL FUNCTIONS ]#########
    ############################## 

    ##############################
    def get_number(tokens, ope, i):
        j = i + 1;
        max = len(ope);
        while (j < max and ope[j].isdigit() == True):
            j += 1;
        if (j < max and ope[j] == "."):
            j += 1
            while (j < max and ope[j].isdigit() == True):
                j += 1;
        tokens.append({"type": "NUMBER", "value": float(ope[i:j])});
        return (j);

    ##############################
    def get_operator(tokens, match):
        tokens.append({"type": "OPERATOR", "value": match});

    ##############################
    def get_sign(tokens, match):
        tokens.append({"type": "SIGN", "value": match});

    ##############################
    def get_new_token(tokens, ope, i):
        operators = "*^="
        signs = "-+";
        char = ope[i];
        if (char.isdigit() == True):
            return (get_number(tokens, ope, i));
        match = operators.find(char);
        if (match >= 0):
            get_operator(tokens, operators[match]);
            return (i + 1);
        match = signs.find(char);
        if (match >= 0):
            get_sign(tokens, signs[match]);
            return (i + 1);
        elif (char == "X"):
            tokens.append({"type": "X"});
            return (i + 1);
        print("ERROR: unknown character [" + char + "]");
        exit(1);

    ############################## 
    ##[ MAIN ]####################
    ############################## 
    ope = re.sub(r"[ \t]*", "", string);
    while (i < len(ope)):
        i = get_new_token(tokens, ope, i);
    return (tokens);
